gdb -q --batch -ex 'info sources' -ex quit tezos-node | sed 's/, /\n/g' | grep '^/'  > /tmp/paths.txt
grep '^/home/tezos/.opam/tezos/.opam-switch/build/' /tmp/paths.txt | grep _build | grep -v default | sed 's/\/_build.*/\/_build/' | sort -u | awk '{ s=$1; gsub(/\/build/, "/sources", s); gsub(/_build/, "src", s) ; print "\"" $1 "\": \"" s "\"," }'
grep '^/home/tezos/.opam/tezos/.opam-switch/build/' /tmp/paths.txt | grep _build | grep default | sed 's/\/_build\/default.*/\/_build\/default/' | sort -u | awk '{ s=$1; gsub(/\/build/, "/sources", s); gsub(/\/_build\/default/, "", s) ; print "\"" $1 "\": \"" s "\"," }'
grep '^/home/tezos/.opam/tezos/.opam-switch/build/' /tmp/paths.txt | grep -v _build | sed -r 's/build\/([^\/]*)\/.*/build\/\1/' | sort -u | awk '{ s=$1; gsub(/\/build/, "/sources", s); print "\"" $1 "\": \"" s "\"," }'
echo '"/workspaces/tezos/_build/default": "/workspaces/tezos",'
echo '"/build/glibc-eX1tMB": "/usr/src/glibc",'
echo '"/build/glibc-iW00TY": "/usr/src/glibc",'
